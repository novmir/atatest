package cz.mn.atatest.service.mapping;

import cz.mn.atatest.model.dto.DbConnection;
import cz.mn.atatest.repository.entity.DbConnectionEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface DbConnectionMapper {
    DbConnection toDTO(DbConnectionEntity entity);
    DbConnectionEntity toEntity(DbConnection dto);
    List<DbConnection> toDTOs(List<DbConnectionEntity> entities);
    List<DbConnectionEntity> toEntities(List<DbConnection> dtos);
}
