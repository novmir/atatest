package cz.mn.atatest.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "These Are Not the Droids You Are Looking For")
public class ResourceNotFoundException extends RuntimeException {
}
