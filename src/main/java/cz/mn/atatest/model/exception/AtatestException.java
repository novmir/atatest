package cz.mn.atatest.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "")
public class AtatestException extends RuntimeException {

    public AtatestException(String msg, Throwable e) {
        super(msg, e);
    }
}
