package cz.mn.atatest.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@ToString
public class DbMetadata {

    private Map<String, String> data = new HashMap<>();
}
