package cz.mn.atatest.controller;

import cz.mn.atatest.model.dto.DbConnection;
import cz.mn.atatest.service.api.DbConnectionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DbConnectionController.class)
public class DbConnectionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DbConnectionService service;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void whenPostDbConnection_thenCreateDbConnection() throws Exception {
        DbConnection dbConnection = new DbConnection();
        dbConnection.setDatabaseName("TEST");
        given(service.create(Mockito.any(DbConnection.class))).willReturn(dbConnection);

        mvc.perform(post("/db").contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(dbConnection)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.databaseName", is("TEST")));
        verify(service, VerificationModeFactory.times(1)).create(Mockito.any(DbConnection.class));
        reset(service);
    }
}