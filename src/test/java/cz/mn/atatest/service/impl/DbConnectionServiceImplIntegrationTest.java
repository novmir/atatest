package cz.mn.atatest.service.impl;

import cz.mn.atatest.model.dto.DbConnection;
import cz.mn.atatest.repository.DbConnectionRepository;
import cz.mn.atatest.repository.entity.DbConnectionEntity;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DbConnectionServiceImplIntegrationTest {

    @InjectMocks
    private DbConnectionServiceImpl dbConnectionService;

    @Mock
    private DbConnectionRepository dbConnectionRepository;

    @BeforeMethod
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenValidId_thenDbConnectionShouldBeFound() {
        DbConnectionEntity dbConnectionEntity = new DbConnectionEntity();
        dbConnectionEntity.setId(1L);
        dbConnectionEntity.setDatabaseName("TEST");

        //when
        when(dbConnectionRepository.findById(dbConnectionEntity.getId()))
                .thenReturn(java.util.Optional.of(dbConnectionEntity));

        DbConnection found = dbConnectionService.get(1L);

        //then
        assertThat(found.getDatabaseName()).isEqualTo("TEST");
    }

}