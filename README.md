# atatest

##### Spring-Boot, Hibernate, JPA, Lombok, Mapstruct, Swagger, Mockito, H2
###Requirement:
Implement a web based database browser with basic functionality and for single database vendor only. Browser
should be able to register multiple database connections and browse their data and structure.

Only for demonstration purposes. 

## How to run it with Docker
Assume you already have Docker installed. See https://docs.docker.com/installation/.

First, clone the project and build locally:

~~~
git clone https://gitlab.com/novmir/atatest.git
cd atatest
mvn clean install
docker build -t mn/atatest:latest .
~~~

Create private network:
~~~
docker network create atatest-net
~~~

Run MySQL 5.6 in Docker container and add it to private network:

~~~
docker run -d --name atatest-mysql -p 33330:3306 --net atatest-net -e MYSQL_ROOT_PASSWORD=atatest-pass -v /data:/var/lib/mysql genschsa/mysql-employees
~~~

Check the log to make sure the server is running OK:
~~~
docker logs atatest-mysql
~~~

Run demo application in Docker container add it to private network:

~~~
docker run -p 33331:8080 --name atatest --net atatest-net -d mn/atatest
~~~

You can check the log by
~~~
docker logs atatest
~~~

#### Swagger
http://localhost:33331/swagger-ui.html
____

#### Sanity Testing
Create new db connection record:
~~~
POST "http://localhost:33331/db"

{
  "databaseName": "employees",
  "hostName": "atatest-mysql",
  "name": "mysql-docker",
  "password": "atatest-pass",
  "port": 3306,
  "userName": "root"
}
~~~

Then list schemas via:
~~~
GET "http://localhost:33331/db/{id}/schemas" 
~~~

Then list tables via:
~~~
GET "http://localhost:33331/db/{id}/{schema}/tables" 
~~~

Then preview data via:
~~~
GET "http://localhost:33331/db/{id}/{schema}/{table}/preview" 
~~~






